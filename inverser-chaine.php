<?php

// Créez un programme qui affiche l’inverse de la chaîne de caractères donnée en argument.

function inverseArray(string $phrase) {
    $array = [];
    for($i=0; $i< strlen($phrase); $i++) {
        $array[]= $phrase[$i];
    }
    return implode(array_reverse($array));
    
}
if($argc === 2 and !is_numeric($argv[1])){
    print inverseArray($argv[1]);
}else{
    print 'erreur';
}
