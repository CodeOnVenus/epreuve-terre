<?php

//function get hour from 12 to 24
function getHour($time) {
    $time = explode(":", $time);
    $hour = $time[0];
    $minute= substr($time[1], 0, 2);
    if ($hour < 12) {
        return $hour + 12 . ":" . $minute . "\n";
    }else{
        return $hour. ":" . $minute. "\n";
    }
}

print getHour($argv[1]);