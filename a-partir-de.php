<?php


function aPartirDe($lettre) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyz';
    return substr($alphabet, strpos($alphabet, $lettre));
}

print aPartirDe($argv[1]);