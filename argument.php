<?php

// Créez un programme qui affiche les arguments qu’il reçoit ligne par ligne, peu importe le nombre d’arguments.

function afficheur(array $arguments) {
    foreach($arguments as $argument) {
        echo $argument . "\n";
    }
}

afficheur($argv);
