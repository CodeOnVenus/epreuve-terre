<?php

// Créez un programme qui transforme une heure affichée en format 24h en une heure affichée en format 12h.

function transform($time) {
    $time = explode(":", $time);
    $hour = $time[0];
    $minute = $time[1];
    if ($hour > 12) {
        $hour = $hour - 12;
    }
    if ($hour < 10) {
        $hour = "0" . $hour;
    }
    if ($minute < 10) {
        $minute = "0" . $minute;
    }
    if($hour > 12) {
        return $hour . ":" . $minute . " PM";
    }else{
        return $hour . ":" . $minute . "AM";
    }
    
}

print transform($argv[1]);