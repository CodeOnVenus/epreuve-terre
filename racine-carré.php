<?php

// Créez un programme qui affiche la racine carrée d’un entier positif.

function racine($num) {
    if(is_numeric($num)) {
        print round(sqrt($num));
    }else{
        print 'erreur';
    }
}

isset($argv[1]) ? racine($argv[1]) : print 'erreur';