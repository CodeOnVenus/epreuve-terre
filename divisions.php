<?php
// Créez un programme qui affiche le résultat et le reste d’une division entre deux nombres.

function division (int $a, int $b) {
    if($b !== "0" and $b <= $a){
        if($a % $b == 0){
            echo 'resultat: ' . $a / $b;
        }else{
            echo 'resultat:' . round($a / $b) . "\n";
            echo 'reste: ' . $a % $b . "\n";
            }
    }else{
        echo 'erreur';
    }
}

echo division($argv[1], $argv[2]);