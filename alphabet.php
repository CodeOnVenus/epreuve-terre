<?php

// Créez un programme qui affiche l’alphabet en lettres minuscules suivi d’un retour à la ligne.
// Attention : votre programme devra utiliser une boucle.

$alphabet = range('A', 'Z');

function minuscule(array $array) {
    foreach($array as $i) {
        echo strtolower($i);
    }
    echo "\n";
}

minuscule($alphabet) ;
