<?php

// Créez un programme qui permet de déterminer si l’argument donné est un entier pair ou impair..

function pairImpair($n) {
    if(is_int($n) or $n < 0 or $n > 0 or $n === "0") {
        if($n%2 === 0) {
            echo 'pair';
        }else{
            echo 'impair';
        }
    }else{
        echo "Ce n'est pas un chiffre";
    }
    
}

echo pairImpair($argv[1]);

