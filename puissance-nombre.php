<?php

// Créez un programme qui affiche le résultat d’une puissance. L’exposant ne pourra pas être négatif


function puissance($a, $b){
    if($a >0 and $b > 0) {
        return pow($a, $b);
    }else{
        echo 'erreur';
    }
}

print puissance($argv[1], $argv[2]);